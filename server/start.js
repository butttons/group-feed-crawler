require('dotenv').config();
const express = require('express');
const app = express();
const path = require('path');
const cron = require('node-cron');

const server = require('http').Server(app);
const bodyParser = require('body-parser');

const cronJob = require('./src/cron/cron-job');

cron.schedule('0 * * * *', cronJob);

app.io = require('socket.io')(server);
server.listen(process.env.PORT || 3000, () =>
    console.log('App listening on port 3000!')
);

app.use(bodyParser.json());
app.use('/static', express.static('dist/static'));

const api = require('./routes/api')(app.io);
app.use('/api', api);

app.io.on('connection', function (socket) {
    socket.emit('log', 'Connected to server');
});

app.get('/', (req, res) =>
    res.sendFile(path.join(__dirname, 'dist', 'index.html'))
);