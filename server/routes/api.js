const express = require('express');
const router = express.Router();
const dbApi = require('./api/database');

module.exports = io => {
    const userApi = require('./api/user')(io);
    const fbApi = require('./api/facebook')(io);

    router.use('/users', userApi);
    router.use('/facebook', fbApi);
    router.use('/database', dbApi);

    return router;
};
