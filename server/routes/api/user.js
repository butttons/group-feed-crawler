const express = require('express');
const router = express.Router();

const {
    getAllUsers,
    deleteUser,
    getGroups
} = require('./../../src/database/functions')();
const facebookLogin = require('./../../src/facebook/login');

router.get('/get', async (req, res) => {
    let allUsers = await getAllUsers();
    res.json(allUsers);
});
router.post('/delete', async (req, res) => {
    let delUser = await deleteUser(req.body.facebookId);
    let allUsers = await getAllUsers();
    res.json(allUsers);
});
router.post('/add', async (req, res) => {
    let status = await facebookLogin(
        req.body.username,
        req.body.password,
        req.body.lang
    );
    if (typeof status.erorr !== 'undefined') {
        res.json(status);
        return;
    }
    let allUsers = await getAllUsers();
    let allGroups = await getGroups(status.facebookId);
    res.json({
        users: allUsers,
        groups: allGroups,
        thisUser: status.id
    });
});
module.exports = socket => {
    return router;
};
