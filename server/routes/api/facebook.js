const express = require('express');
const router = express.Router();
const {
    getPosts,
    formattedKeywords,
    upsertSetting
} = require('./../../src/database/functions')();
const moment = require('moment');
router.get('/posts', async (req, res) => {
    let posts = await getPosts();
    res.json(posts);
});

module.exports = socket => {
    const {
        fetchGroupPosts
    } = require('./../../src/facebook/crawler')(socket);

    router.post('/crawl', async (req, res) => {
        try {
            const groups = req.body.groups;
            const user = req.body.user;
            const endDate = req.body.endDate;

            const keywords = await formattedKeywords();
            const endDateFormatted = endDate === null ? 'eternity' : endDate;
            socket.emit('log', `Starting crawler`);
            socket.emit('log', `Searching till - ${endDateFormatted}`);
            groups.forEach(async group => {
                const groupKeywords = keywords[group.group_id];
                if (typeof groupKeywords !== 'undefined') {
                    socket.emit('log', `Getting posts of group: ${group.group_id}`);
                    const groupPosts = await fetchGroupPosts(
                        group,
                        user,
                        endDate,
                        keywords[group.group_id]
                    );
                }
            });
            await upsertSetting('endDate', moment().format());
            res.json({
                text: 'Updated'
            });
        } catch (err) {
            console.log('Crawling error:', err);
        }
    });
    return router;
};