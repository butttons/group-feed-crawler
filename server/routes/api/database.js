const express = require('express');
const router = express.Router();
const {
    upsertKeyword,
    getKeywords,
    getGroups,
    deleteKeyword,
    updateGroupCrawl,
    getSetting
} = require('./../../src/database/functions')();

router.post('/keywords/add', async (req, res) => {
    const keyword = req.body.keyword;
    const groupId = req.body.groupId;
    const keywordUpsert = await upsertKeyword(keyword, groupId);
    const keywordStatus =
        typeof keywordUpsert === 'number' ?
        `Already added "${keyword}"` :
        `Added new keyword "${keyword}"`;
    const allKeywords = await getKeywords();
    res.json({
        text: keywordStatus,
        keywords: allKeywords
    });
});
router.get('/keywords/get', async (req, res) => {
    const allKeywords = await getKeywords();
    res.json({
        keywords: allKeywords
    });
});
router.post('/keywords/delete', async (req, res) => {
    await deleteKeyword(req.body.id);
    const allKeywords = await getKeywords();
    res.json({
        keywords: allKeywords
    });
});

router.get('/groups/all', async (req, res) => {
    const allGroups = await getGroups();
    res.json(allGroups);
});

router.get('/groups/:user_id', async (req, res) => {
    const allGroups = await getGroups(req.params.user_id);
    res.json(allGroups);
});

router.post('/groups/update', async (req, res) => {
    const groups = await updateGroupCrawl(req.body.groups, req.body.userId);
    res.json(groups);
});

router.get('/settings', async (req, res) => {
    const endDateSetting = await getSetting('endDate');
    res.json({
        endDate: endDateSetting ? endDateSetting : null
    });
});
module.exports = router;