const {
    REQ_HEADERS,
    BASE_URL,
    SQL_DATE_FORMAT
} = require('./request-options');
const fs = require('fs');
const crawlerFactory = deps => {
    const langOptions = {
        en: {
            seeMore: 'See more posts',
            fullStory: 'FullStory',
            likeLink: 'Like',
            yesterday: 'Yesterday'
        },
        it: {
            seeMore: 'Vedi altri post',
            fullStory: 'Notiziacompleta',
            likeLink: 'piace',
            yesterday: 'Ieri'
        }
    };
    const getHours = (amPm, hours) => {
        return amPm.length ? (amPm === 'pm' ? +hours + 12 : +hours) : +hours;
    };
    const keywordsMatch = (text, keywords) => {
        const matches = keywords
            .map(keyword => (text.toLowerCase().indexOf(keyword.toLowerCase()) > -1 ? keyword : false))
            .filter(match => !!match);
        return !matches.length ? null : matches.join(',');
    };
    const parsePostTime = (time, locale = 'en') => {
        let parsedTime = false;

        const dayMatch = time.match(
            /(\d+) ([^\s]+)\s?(\d*)\s?(at|alle)\s?([ore]{0,3})\s?(\d+):(\d+)([apm]{0,2})/
        );

        if (dayMatch) {
            const hours = getHours(dayMatch[8], dayMatch[6]);
            const mins = +dayMatch[7];
            parsedTime = deps
                .moment()
                .set('year', dayMatch[3].length ? +dayMatch[3] : deps.moment().year())
                .set('date', +dayMatch[1])
                .set('month', dayMatch[2])
                .set('hour', hours)
                .set('minute', mins);
        }

        const weekdayMatch = time.match(
            /^([^\s]+) (alle|at) (\d+):(\d+)([apm]{0,2})/
        );
        if (weekdayMatch && !parsedTime) {
            const hours = getHours(weekdayMatch[5], weekdayMatch[3]);
            const mins = +weekdayMatch[4];
            let dayMoment =
                weekdayMatch[1] === langOptions[locale].yesterday ?
                deps.moment().subtract(1, 'day') :
                deps.moment().day(weekdayMatch[1]);
            parsedTime = dayMoment.set('hour', hours).set('minute', mins);
        }
        let sec = time.match(/^(\d+) s/);
        if (sec && !parsedTime) {
            parsedTime = deps.moment().subtract(+sec[1], 'seconds');
        }
        let min = time.match(/^(\d+) m/);
        if (min && !parsedTime) {
            parsedTime = deps.moment().subtract(+min[1], 'minutes');
        }
        let hr = time.match(/^(\d+) h/);
        if (hr && !parsedTime) {
            parsedTime = deps.moment().subtract(+hr[1], 'hours');
        }
        return parsedTime ? parsedTime.format(SQL_DATE_FORMAT) : time;
    };
    const parseCommentTime = (time, locale = 'en') => {
        let parsedTime = false;

        const dayMatch = time.match(
            /((\d+) ([^\s]+)) (alle|at) (\d+):(\d+)([apm]{0,2})/
        );
        if (dayMatch) {
            const hours = getHours(dayMatch[7], dayMatch[5]);
            const mins = +dayMatch[6];
            parsedTime = deps
                .moment()
                .set('date', +dayMatch[2])
                .set('month', dayMatch[3])
                .set('hour', hours)
                .set('minute', mins);
        }
        const secMinHr = time.match(/(\d+) (\w+) (ago|fa)/);
        if (secMinHr && !parsedTime) {
            parsedTime = deps.moment().subtract(+secMinHr[1], secMinHr[2]);
        }
        const weekdayMatch = time.match(
            /^([^\s]+) (alle|at) (\d+):(\d+)([apm]{0,2})/
        );
        if (weekdayMatch && !parsedTime) {
            const hours = getHours(weekdayMatch[5], weekdayMatch[3]);
            const mins = +weekdayMatch[4];
            let dayMoment =
                weekdayMatch[1] === langOptions[locale].yesterday ?
                deps.moment().subtract(1, 'day') :
                deps.moment().day(weekdayMatch[1]);
            parsedTime = dayMoment.set('hour', hours).set('minute', mins);
        }
        const yearMatch = time.match(/20[\d]{2}/);
        if (!yearMatch && !parsedTime) {
            parsedTime = deps.moment(
                new Date(`${time} ${deps.moment().year()}`)
            );
        }
        const fullDateMatch = time.match(/(\d+) ([^\s]+) (\d+)/);
        if (fullDateMatch) {
            parsedTime = deps.moment().set('year', +fullDateMatch[3])
                .set('date', +fullDateMatch[1])
                .set('month', fullDateMatch[2])
        }
        return parsedTime ? parsedTime.format(SQL_DATE_FORMAT) : time;
    };
    const genRequestOptions = (uri, user) => {
        return {
            method: 'GET',
            uri,
            headers: {
                cookie: user.cookie,
                'user-agent': user.user_agent || user.userAgent,
                ...REQ_HEADERS
            },
            simple: false,
            transform: body => deps.cheerio.load(body)
        };
    };
    return (socket = false) => {
        const fetchUserGroups = async user => {
            let rpOps = genRequestOptions(
                `${BASE_URL}/groups/?seemore&refid=27`,
                user
            );
            let $ = await deps.rp(rpOps);
            let groupLinks = $('a')
                .map((k, a) => {
                    return {
                        href: $(a).attr('href'),
                        name: $(a).text()
                    };
                })
                .get()
                .filter(a => a.href.match(/groups\/\d+/))
                .map(a => {
                    return {
                        id: a.href.match(/groups\/(\d+)/)[1],
                        ...a
                    };
                });
            return groupLinks;
        };

        const fetchGroupPosts = async (
            group,
            user,
            endDate,
            keywords,
            accumulator = [],
            url = false
        ) => {
            try {
                if (typeof keywords === 'undefined') return `No keywords defined for ${group.name}`
                let rpOps = genRequestOptions(
                    url || `${BASE_URL}/groups/${group.group_id}`,
                    user
                );
                let $ = await deps.rp(rpOps);
                let seeMore = $('a')
                    .filter(
                        (i, el) => $(el).text() === langOptions[user.lang].seeMore
                    )
                    .attr('href');

                let groupPosts = $('div[role=article]')
                    .map((k, div) => {
                        let posterLink = $(div)
                            .find('strong')
                            .find('a');
                        let postText = $(div)
                            .find('div[class^=d]')
                            .find('div[data-ft]')
                            .text()
                            .replace(/\n\s+/g, '');
                        let postTime = $(div)
                            .find($('div[data-ft]'))
                            .find('abbr')
                            .text();
                        let postLinks = $(div)
                            .find('a')
                            .map((k, el) => {
                                return {
                                    text: $(el)
                                        .text()
                                        .replace(/\s+/g, ''),
                                    url: $(el).attr('href'),
                                    class: $(el).attr('class'),
                                    aria: $(el).attr('aria-label')
                                };
                            })
                            .get();
                        let fullStory = postLinks.filter(
                            p => p.text === langOptions[user.lang].fullStory
                        );
                        let likeCount = postLinks.filter(
                            p =>
                            p.aria ?
                            p.aria.indexOf(
                                langOptions[user.lang].likeLink
                            ) > -1 ?
                            true :
                            false :
                            false
                        );
                        let commentLink = postLinks.filter(p =>
                            p.text.match(/\d+Comment/i)
                        );
                        let commentCount = commentLink.length ?
                            +commentLink[0].text.match(/(\d+)Comment/i)[1] :
                            0;
                        if (fullStory.length) {
                            const matchingKeywords = keywordsMatch(
                                postText,
                                keywords
                            );
                            let post = {
                                post_text: postText,
                                post_time_raw: postTime,
                                post_time: parsePostTime(postTime, user.lang),
                                poster: {
                                    name: posterLink.text(),
                                    url: posterLink.attr('href')
                                },
                                keywords_match: matchingKeywords,
                                post_url: fullStory[0].url,
                                post_id: fullStory[0].url.match(/id=(\d+)/)[1],
                                likes: likeCount.length ? +likeCount[0].text : 0,
                                comments: commentCount,
                                group_id: group.group_id,
                                comments_cached: matchingKeywords === null ? 0 : 1
                            };
                            deps.upsertPost(post, user).then(didUpdate => {
                                if (post.comments_cached) {
                                    if (!didUpdate) {
                                        fetchPostComments(user, post).then(
                                            comments => {
                                                deps.airtable
                                                    .addRecord({
                                                        group_name: group.name,
                                                        ...post
                                                    }, comments)
                                                    .then(airAdd => {
                                                        if (socket) socket.emit(
                                                            'log',
                                                            'Added to airtable'
                                                        );
                                                    });
                                            }
                                        );
                                        if (socket)
                                            socket.emit('log', `Fetching comments of #${post.post_id}, found keywords: ${matchingKeywords}`);
                                    } else {
                                        if (socket)
                                            socket.emit('log', `Already saved comments of #${post.post_id}`);
                                    }
                                }
                            });
                            return post;
                        }
                    })
                    .get();
                const matchingPosts = groupPosts.filter(p => p.keywords_match !== null);
                accumulator = accumulator.concat(matchingPosts);
                if (socket)
                    socket.emit('log', `Cached ${groupPosts.length} posts`);
                if (typeof seeMore !== 'undefined') {
                    if (endDate === null) {
                        return await fetchGroupPosts(
                            group,
                            user,
                            endDate,
                            keywords,
                            accumulator,
                            `${BASE_URL}${seeMore}`
                        );
                    } else {
                        if (
                            deps
                            .moment(endDate)
                            .isBefore(groupPosts.slice(-1)[0].post_time)
                        ) {
                            return fetchGroupPosts(
                                group,
                                user,
                                endDate,
                                keywords,
                                accumulator,
                                `${BASE_URL}${seeMore}`
                            );
                        } else {
                            if (socket) {
                                socket.emit('log', `Ending crawling`);
                                socket.emit('setSpin', false);
                            }
                            return accumulator;
                        }
                    }
                } else {
                    if (socket) {
                        socket.emit('log', `No posts found. Ending crawling`);
                        socket.emit('setSpin', false);
                    }
                    return accumulator;
                }
            } catch (err) {
                console.log(err);
            }
        };
        const fetchPostComments = async (
            user,
            post,
            accumulator = [],
            nextUrl = false
        ) => {
            let mainUrl = !nextUrl ?
                `${BASE_URL}${post.post_url}` :
                `${BASE_URL}${nextUrl}`;
            let commentOps = genRequestOptions(mainUrl, user);
            let $ = await deps.rp(commentOps);
            let seePrev = $('div[id^=see_prev]')
                .find('a')
                .attr('href');
            let postComments = $('div[class$=x], div[class$=u]')
                .map((k, div) => {
                    let _id = $(div).attr('id');
                    if (_id && _id.match(/(\d){15,}/)) {
                        let commentText = $(div)
                            .find('h3')
                            .next()
                            .text();
                        let userPost = $(div)
                            .find('h3')
                            .find('a');
                        let commentTime = $(div)
                            .find('abbr')
                            .text();
                        if (commentText.length)
                            return {
                                comment_id: _id,
                                post_id: post.post_id,
                                time: commentTime,
                                comment_time: parseCommentTime(
                                    commentTime,
                                    user.lang
                                ),
                                text: commentText,
                                name: userPost.text(),
                                url: userPost.attr('href')
                            };
                    }
                })
                .get();
            accumulator = accumulator.concat(postComments);
            if (!seePrev) {
                await deps.upsertComments(accumulator, user);
                return accumulator;
            } else {
                return await fetchPostComments(
                    user,
                    post,
                    accumulator,
                    seePrev
                );
            }
        };
        return {
            fetchUserGroups,
            fetchGroupPosts,
            fetchPostComments,
            parseCommentTime,
            parsePostTime,
            keywordsMatch
        };
    };
};

module.exports = crawlerFactory;