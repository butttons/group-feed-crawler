const REQ_HEADERS = {
    accept:
        'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'identity',
    'accept-language':
        'en-IN,en;q=0.9,hi-IN;q=0.8,hi;q=0.7,en-GB;q=0.6,en-US;q=0.5',
    'cache-control': 'no-cache',
    dnt: 1,
    pragma: 'no-cache',
    'upgrade-insecure-requests': 1
};
const BASE_URL = 'https://mbasic.facebook.com';
const SQL_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

module.exports = { REQ_HEADERS, BASE_URL, SQL_DATE_FORMAT };
