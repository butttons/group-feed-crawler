const crawler = require('./crawler')();
const {
    upsertPosts,
    upsertGroups,
    getUser,
    upsertSetting,
    getKeywords,
    areCommentsCached
} = require('./../database/functions')();

const fs = require('fs');

(async () => {
    /*
    let px = crawler.parseCommentTime('27 December 2017');
    console.log(px);
    */
    let user = await getUser('Insannne');

    if (user) {
        let comms = await crawler.fetchPostComments(user, {
            post_id: '301367079936237',
            post_url: '/groups/301367079936237?view=permalink&id=305330456206566'
        });
        console.log(comms);
    }

})();