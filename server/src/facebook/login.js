const cheerio = require('cheerio');
const qs = require('qs');
const tough = require('tough-cookie');
const Cookie = tough.Cookie;
const rp = require('request-promise');
const fakeUa = require('fake-useragent');

const { upsertUser, upsertGroups } = require('./../database/functions')();
const { fetchUserGroups } = require('./crawler')(false);
module.exports = require('./login.factory')({
    cheerio,
    qs,
    upsertUser,
    upsertGroups,
    fetchUserGroups,
    Cookie,
    rp,
    fakeUa
});
