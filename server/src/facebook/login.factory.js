const { REQ_HEADERS } = require('./request-options');

const login = deps => {
    const parseSetCookie = cookies => {
        return cookies
            .map(deps.Cookie.parse)
            .filter(ck => ck.value !== 'deleted')
            .map(ck => ck.cookieString())
            .join('; ');
    };
    const fetchPostOptions = async userAgent => {
        const FB_URL = 'https://mbasic.facebook.com/';
        let loginPage = await deps.rp({
            uri: FB_URL,
            method: 'GET',
            resolveWithFullResponse: true,
            headers: { 'user-agent': userAgent, ...REQ_HEADERS }
        });
        let $ = deps.cheerio.load(loginPage.body);
        let postAction = $('#login_form').attr('action');
        let loginPost = {
            lsd: $('input[name=lsd]').attr('value'),
            m_ts: $('input[name=m_ts]').attr('value'),
            li: $('input[name=li]').attr('value'),
            try_number: 0,
            unrecognized_tries: 0
        };
        let _ck = loginPage.headers['set-cookie'].map(deps.Cookie.parse);
        return {
            cookie: _ck.map(ck => ck.cookieString()).join('; '),
            action: postAction,
            post: loginPost
        };
    };
    return async (username, password, lang) => {
        let newUa = deps.fakeUa();
        let postOptions = await fetchPostOptions(newUa);

        let postData = {
            email: username,
            pass: password,
            ...postOptions.post
        };
        let postForm = deps.qs.stringify(postData);
        let loginFormOp = {
            method: 'POST',
            uri: postOptions.action,
            form: postData,
            headers: {
                'content-length': postForm.length,
                cookie: postOptions.cookie,
                'user-agent': newUa,
                ...REQ_HEADERS
            },
            resolveWithFullResponse: true,
            simple: false
        };

        let loginForm = await deps.rp(loginFormOp);

        let isBlocked = loginForm.headers['location']
            ? loginForm.headers['location'].match(/checkpoint\//)
                ? true
                : false
            : false;
        if (isBlocked) {
            return { error: 'Account blocked' };
        }
        let isUser = loginForm.headers['set-cookie']
            .map(deps.Cookie.parse)
            .filter(ck => ck.key === 'c_user');

        if (isUser.length) {
            let finalCookie = parseSetCookie(loginForm.headers['set-cookie']);
            let _user = {
                facebookId: isUser[0].value,
                username,
                password,
                cookie: finalCookie,
                userAgent: newUa
            };
            let groupData = await deps.fetchUserGroups(_user);
            await deps.upsertGroups(groupData, _user);
            const newUserId = await deps.upsertUser(_user, lang);
            return { id: newUserId, ..._user };
        } else {
            return { error: 'Login failed' };
        }
    };
};
module.exports = login;
