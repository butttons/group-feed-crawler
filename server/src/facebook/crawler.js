const rp = require('request-promise');
const cheerio = require('cheerio');
const { db } = require('./../database/main');
const {
    upsertPosts,
    upsertPost,
    upsertSetting,
    upsertComments,
    getPost,
    areCommentsCached
} = require('./../database/functions')();
const airtable = require('./../airtable/airtable')();
const moment = require('moment');

module.exports = require('./crawler.factory')({
    db,
    rp,
    cheerio,
    upsertPosts,
    upsertSetting,
    upsertComments,
    areCommentsCached,
    upsertPost,
    getPost,
    moment,
    airtable
});
