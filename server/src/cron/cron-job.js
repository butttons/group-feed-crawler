require('dotenv').config();
const {
    fetchGroupPosts
} = require('./../../src/facebook/crawler')(false);
const {
    getFirstUser,
    formattedKeywords,
    getCrawlingGroups,
    getEndDate,
    emailedPosts,
    updateEmailPosts
} = require('./../../src/database/functions')();
const {
    sendEmail
} = require('./mailer');

module.exports = require('./cron-job.factory')({
    fetchGroupPosts,
    getCrawlingGroups,
    getEndDate,
    getFirstUser,
    formattedKeywords,
    emailedPosts,
    sendEmail,
    updateEmailPosts
})