const mailerFactory = deps => {
    const createEmailText = allPosts => {
        const emailText = [];
        let groupCount = 0;
        let postCount = 0;
        for (let groupName in allPosts) {
            let posts = allPosts[groupName];
            emailText.push(`Group: ${groupName}`);
            emailText.push(createPostList(posts));
            emailText.push('-'.repeat(20));
            postCount += posts.length;
            groupCount++;
        }
        return postCount ? {
            subject: `group-feed-crawler: ${postCount} posts in ${groupCount} groups`,
            text: emailText.join('\n')
        } : false;
    }
    const createPostList = groupPosts => {
        return groupPosts.map(post => `Keywords: ${post.keywords}\nURL: ${post.url}\nText: ${post.text}`).join('\n');
    }
    const sendEmail = async (allPosts) => {
        const emailBody = createEmailText(allPosts);
        const emailAddress = process.env.GMAIL_USERNAME;
        if (!emailBody) return 'No new posts found';
        return await deps.transporter.sendMail({
            to: emailAddress,
            from: emailAddress,
            ...emailBody
        });
    }
    const verifyEmail = () => {
        deps.transporter.verify(function (error, success) {
            if (error) {
                console.log(error);
            } else {
                console.log('Server is ready to take our messages');
            }
        });
    }
    return {
        sendEmail,
        verifyEmail
    }
};
module.exports = mailerFactory;