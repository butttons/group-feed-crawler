require('dotenv').config();
const nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
    port: 587,
    host: 'smtp.gmail.com',
    auth: {
        user: process.env.GMAIL_USERNAME,
        pass: process.env.GMAIL_PASSWORD
    }
});
module.exports = require('./mailer.factory')({
    transporter
});