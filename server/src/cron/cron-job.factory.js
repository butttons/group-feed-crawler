const cronFactory = deps => {
    return async () => {
        console.log('Sending email');
        const groups = await deps.getCrawlingGroups();
        const keywords = await deps.formattedKeywords();
        const user = await deps.getFirstUser();
        const emailedPosts = await deps.emailedPosts();
        let finalPosts = [];
        for (group of groups) {
            finalPosts[group.group_id] = finalPosts[group.group_id] || [];
            const endDate = await deps.getEndDate(group.group_id);
            console.log('endDate', endDate, group.group_id);
            const groupKeywords = keywords[group.group_id];
            const groupPosts = await deps.fetchGroupPosts(
                group,
                user,
                endDate,
                keywords[group.group_id]
            );
            const emailPosts = groupPosts.map(p => {
                return {
                    postId: p.post_id,
                    url: `https://www.facebook.com/groups/${p.group_id}/permalink/${p.post_id}/`,
                    text: p.post_text,
                    keywords: p.keywords_match,
                    groupName: group.name
                }
            }).filter(p => !emailedPosts.includes(p.postId));
            finalPosts = finalPosts.concat(emailPosts);
        }
        const postIds = finalPosts.map(p => p.postId);
        const emailText = finalPosts.reduce((email, post) => {
            email[post.groupName] = email[post.groupName] || [];
            email[post.groupName].push(post)
            return email;
        }, {});
        const emailStatus = await deps.sendEmail(emailText);
        console.log(emailStatus);
        await deps.updateEmailPosts(postIds);
        return emailText;
    }
}
module.exports = cronFactory;