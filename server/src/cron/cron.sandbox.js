const cron = require('./cron-job');
const mailer = require('./mailer');

(async () => {
    const posts = await cron();
    console.log(posts);
})();