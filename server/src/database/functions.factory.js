const SQL_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';
const functionsFactory = deps => {
    const upsertUser = async (user, lang) => {
        let updateUser = await deps
            .db('users')
            .where({
                facebook_id: user.facebookId || user.facebook_id
            })
            .update({
                cookie: user.cookie,
                lang,
                updated_at: deps.moment().format(SQL_DATE_FORMAT)
            });

        if (!updateUser) {
            updateUser = await deps.db('users').insert({
                facebook_id: user.facebookId || user.facebook_id,
                username: user.username,
                password: user.password,
                cookie: user.cookie,
                user_agent: user.userAgent || user.user_agent,
                lang,
                working: 1
            });
            let userId = updateUser[0];
        }
        let userObj = await deps
            .db('users')
            .select('id')
            .where({
                facebook_id: user.facebookId || user.facebook_id
            });

        return !updateUser ? userId : userObj[0].id;
    };
    const upsertGroups = async (groups, user) => {
        groups.forEach(async group => {
            let updateGroup = await deps
                .db('groups')
                .where({
                    group_id: group.id
                })
                .update({
                    updated_at: deps.moment().format(SQL_DATE_FORMAT)
                });
            if (!updateGroup) {
                updateGroup = await deps.db('groups').insert({
                    member_facebook_id: user.facebookId || user.facebook_id,
                    group_id: group.id,
                    name: group.name,
                    crawl: 0
                });
            }
        });
    };

    const upsertPosts = async (groupPosts, user) => {
        groupPosts.forEach(async post => {
            if (!post.post_id.length) return;
            let updatePost = await deps
                .db('posts')
                .where({
                    post_id: post.post_id
                })
                .update({
                    likes: post.likes,
                    comments: post.comments,
                    group_id: post.group_id,
                    keywords_match: post.keywords_match,
                    updated_at: deps.moment().format(SQL_DATE_FORMAT),
                    comments_cached: post.comments_cached,
                    post_time: post.post_time
                });
            if (!updatePost) {
                updatePost = await deps.db('posts').insert({
                    found_by: user.facebook_id,
                    post_id: post.post_id,
                    group_id: post.group_id,
                    post_url: post.post_url,
                    post_text: post.post_text,
                    post_time: post.post_time,
                    keywords_match: post.keywords_match,
                    poster_name: post.poster.name,
                    poster_url: post.poster.url,
                    likes: post.likes,
                    comments: post.comments,
                    comments_cached: post.comments_cached
                });
            }
        });
    };
    const upsertPost = async (post, user) => {
        if (!post.post_id.length) return;
        let updatePost = await deps
            .db('posts')
            .where({
                post_id: post.post_id
            })
            .update({
                likes: post.likes,
                comments: post.comments,
                group_id: post.group_id,
                keywords_match: post.keywords_match,
                updated_at: deps.moment().format(SQL_DATE_FORMAT),
                comments_cached: post.comments_cached,
                post_time: post.post_time
            });
        if (!updatePost) {
            updatePost = await deps.db('posts').insert({
                found_by: user.facebook_id,
                post_id: post.post_id,
                group_id: post.group_id,
                post_url: post.post_url,
                post_text: post.post_text,
                post_time: post.post_time,
                keywords_match: post.keywords_match,
                poster_name: post.poster.name,
                poster_url: post.poster.url,
                likes: post.likes,
                comments: post.comments,
                comments_cached: post.comments_cached,
                emailed: 0
            });
        }
        return typeof updatePost === 'number';
    };
    const upsertComments = async (comments, user) => {
        comments.forEach(async comment => {
            let updateComment = await deps
                .db('comments')
                .where({
                    comment_id: comment.comment_id
                })
                .update({
                    updated_at: deps.moment().format(SQL_DATE_FORMAT),
                    comment_time: comment.comment_time
                });
            if (!updateComment) {
                updateComment = await deps.db('comments').insert({
                    comment_id: comment.comment_id,
                    post_id: comment.post_id,
                    text: comment.text,
                    user_name: comment.name,
                    user_url: comment.url,
                    comment_time: comment.comment_time
                });
            }
            return updateComment;
        });
    };
    const getAllUsers = async () => {
        return await deps.db('users').select('*');
    };
    const getUser = async username => {
        let user = await deps
            .db('users')
            .select('*')
            .where({
                username
            });
        return user.length ? user[0] : false;
    };
    const getFirstUser = async () => {
        const users = await getAllUsers();
        return users.length ? users[0] : false;
    };
    const deleteUser = async facebookId => {
        return await deps
            .db('users')
            .where({
                facebook_id: facebookId
            })
            .del();
    };
    const getPosts = async () => {
        const keywordsDb = await getKeywords();
        let outPosts = [];
        for (const keyword of keywordsDb) {
            const posts = await deps
                .db('posts')
                .innerJoin('groups', function () {
                    this.on('groups.group_id', '=', 'posts.group_id');
                })
                .where('posts.group_id', keyword.group_id)
                .andWhere('posts.post_text', 'like', `%${keyword.keyword}%`);
            posts.forEach(p => (p.keyword = keyword.keyword));
            outPosts = outPosts.concat(Object.values(posts));
        }

        outPosts.forEach(p => {
            (p['group_name'] = p['name']), delete p['name'];
            p['post_url'] = `https://www.facebook.com/groups/${
                p.group_id
            }/permalink/${p.post_id}/`;
        });

        return outPosts;
    };
    const getPost = async postId => {
        const posts = await deps
            .db('posts')
            .select('*')
            .where({
                post_id: postId
            });
        return posts[0];
    };
    const upsertKeyword = async (keyword, groupId, keywordType = 'cron') => {
        keyword = keyword.toLowerCase();
        let updateKeyword = await deps
            .db('keywords')
            .where({
                keyword,
                group_id: groupId
            })
            .update({
                group_id: groupId,
                type: keywordType,
                updated_at: deps.moment().format(SQL_DATE_FORMAT)
            });
        if (!updateKeyword) {
            updateKeyword = await deps.db('keywords').insert({
                group_id: groupId,
                type: keywordType,
                keyword,
                hits: 0
            });
        }
        return updateKeyword;
    };
    const getKeywords = async () => {
        return await deps
            .db('keywords')
            .select([
                'keywords.id',
                'groups.name',
                'keywords.keyword',
                'keywords.group_id'
            ])
            .innerJoin('groups', function () {
                this.on('groups.group_id', '=', 'keywords.group_id');
            });
    };
    const formattedKeywords = async () => {
        let keywords = await getKeywords();
        return keywords.reduce((obj, keyword) => {
            obj[keyword.group_id] = obj[keyword.group_id] || [];
            obj[keyword.group_id].push(keyword.keyword);
            return obj;
        }, {});
    };
    const deleteKeyword = async id => {
        return await deps
            .db('keywords')
            .where({
                id
            })
            .del();
    };
    const getGroups = async (userId = false) => {
        return !userId ?
            await deps.db('groups').select('*') :
            await deps
            .db('groups')
            .select('*')
            .where({
                member_facebook_id: userId
            });
    };
    const getCrawlingGroups = async (userId = false) => {
        return !userId ?
            await deps.db('groups').select('*').where({
                crawl: 1
            }) :
            await deps.db('groups').select('*').where({
                crawl: 1,
                member_facebook_id: userId
            })
    }
    const updateGroupCrawl = async (groupIds, userId) => {
        await deps
            .db('groups')
            .update({
                crawl: 0
            })
            .where({
                member_facebook_id: userId
            });
        return await deps
            .db('groups')
            .whereIn('id', groupIds)
            .update({
                crawl: 1
            });
    };
    const upsertSetting = async (config, val) => {
        let updateSetting = await deps
            .db('settings')
            .update({
                val
            })
            .where({
                config
            });
        if (!updateSetting) {
            updateSetting = await deps.db('settings').insert({
                config,
                val
            });
        }
        return updateSetting;
    };
    const getEndDate = async (groupId) => {
        const posts = await deps.db('posts').orderBy('post_time', 'DESC').where({
            group_id: groupId
        });
        return posts.length ? deps.moment(posts[0].post_time).format() : deps.moment().format();
    }
    const getSetting = async config => {
        const setting = await deps
            .db('settings')
            .select('val')
            .where({
                config
            });
        return setting.length ? setting[0].val : false;
    };
    const areCommentsCached = async postId => {
        const post = await deps
            .db('posts')
            .select('comments_cached')
            .where({
                post_id: postId
            });
        return post.length ? !!post[0]['comments_cached'] : false;
    };
    const emailedPosts = async () => {
        const posts = await deps.db('posts').select('post_id').where({
            emailed: 1
        });
        return posts.map(p => p.post_id);
    }
    const updateEmailPosts = async (postIds) => {
        return await deps.db('posts').update({
            emailed: 1
        }).whereIn('post_id', postIds)
    }
    return () => {
        return {
            upsertUser,
            upsertGroups,
            upsertPosts,
            upsertPost,
            upsertComments,
            upsertKeyword,
            upsertSetting,
            updateGroupCrawl,
            getUser,
            getAllUsers,
            getFirstUser,
            getPosts,
            getPost,
            getGroups,
            getKeywords,
            getSetting,
            getEndDate,
            getCrawlingGroups,
            deleteUser,
            deleteKeyword,
            areCommentsCached,
            formattedKeywords,
            emailedPosts,
            updateEmailPosts
        };
    };
};
module.exports = functionsFactory;