const {
    db
} = require('./main');
const {
    upsertSetting
} = require('./functions')();
(async () => {
    console.log('Creating database schema');
    try {
        let userTable = await db.schema.createTable('users', table => {
            table.increments();
            table.string('facebook_id');
            table.string('username');
            table.string('password');
            table.string('cookie');
            table.string('lang');
            table.string('user_agent');
            table.integer('working');
            table.timestamps(true, true);
        });

        let postTable = await db.schema.createTable('posts', table => {
            table.increments();
            table.string('found_by');
            table.string('keywords_match');
            table.string('post_id');
            table.string('group_id');
            table.string('post_url');
            table.dateTime('post_time');
            table.string('post_text');
            table.string('poster_name');
            table.string('poster_url');
            table.integer('comments');
            table.integer('likes');
            table.integer('comments_cached');
            table.integer('emailed');
            table.timestamps(true, true);
        });

        let keywordTable = await db.schema.createTable('keywords', table => {
            table.increments();
            table.string('type');
            table.string('keyword');
            table.integer('hits');
            table.string('group_id');
            table.timestamps(true, true);
        });
        let groupTable = await db.schema.createTable('groups', table => {
            table.increments();
            table.string('name');
            table.string('group_id');
            table.string('member_facebook_id');
            table.integer('crawl');
            table.timestamps(true, true);
        });

        let settingTable = await db.schema.createTable('settings', table => {
            table.increments();
            table.string('config');
            table.string('val');
            table.timestamps(true, true);
        });

        let commentTable = await db.schema.createTable('comments', table => {
            table.increments();
            table.string('comment_id');
            table.string('post_id');
            table.string('text');
            table.string('user_name');
            table.string('user_url');
            table.string('comment_time');
            table.timestamps(true, true);
        });
    } catch (err) {
        console.log(err);
    }
    console.log('Done');
    process.exit();
})();