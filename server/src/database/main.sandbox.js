const { db } = require('./main');

db('posts')
    .insert({
        keywords_match: null,
        post_id: 123
    })
    .then(out => console.log(out));
