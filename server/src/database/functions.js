const { db } = require('./main');
const moment = require('moment');

module.exports = require('./functions.factory')({ db, moment });
