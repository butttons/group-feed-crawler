require('dotenv').config();
const knex = require('knex')({
  client: 'sqlite3',
  useNullAsDefault: true,
  connection: {
    filename: './sqlite/database.sqlite'
  }
});

module.exports = { db: knex };
