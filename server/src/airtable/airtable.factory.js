const {
    SQL_DATE_FORMAT
} = require('./../facebook/request-options');
const airtableFactory = deps => {
    const normalizeUrl = url => url.match(/^\/([^\?]+)/)[0];
    const addComment = async (comment, airPostId) => {
        const airComment = await deps.base('Comments').create({
            'Comment ID': comment.comment_id,
            Text: comment.text,
            'Poster name': comment.name,
            'Poster URL': `fb.me${normalizeUrl(comment.url)}`,
            Posts: [airPostId],
            'Comment time': deps
                .moment(comment.comment_time, SQL_DATE_FORMAT)
                .format()
        });
        return airComment.id;
    };
    const addPost = async post => {
        const airPost = await deps.base('Posts').create({
            'Post ID': post.post_id,
            Text: post.post_text,
            'Poster name': post.poster.name.replace(post.group_name, ''),
            'Poster URL': `fb.me${normalizeUrl(post.poster.url)}`,
            Group: post.group_name,
            'Comment count': post.comments,
            'Like count': post.likes,
            Keyword: post.keywords_match,
            'Post time': deps.moment(post.post_time, SQL_DATE_FORMAT).format(),
            'Post URL': `https://www.facebook.com/groups/${post.group_id}/permalink/${post.post_id}/`
        });
        return airPost.id;
    };
    const addRecord = async (post, comments) => {
        const airPostId = await addPost(post);
        const commentIds = [];
        for (comment of comments) {
            const airCommentId = await addComment(comment, airPostId);
            commentIds.push(airCommentId);
        }
        const updateP = await deps
            .base('Posts')
            .update(airPostId, {
                Comments: commentIds
            });
        return updateP;
    };
    return () => {
        return {
            addRecord
        };
    };
};
module.exports = airtableFactory;