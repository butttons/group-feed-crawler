require('dotenv').config();
const moment = require('moment');
const path = require('path');
const Airtable = require('airtable');
const base = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }).base(
    process.env.AIRTABLE_API_BASE
);
module.exports = require('./airtable.factory')({ base, moment });
