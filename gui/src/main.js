// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';
import VueAxios from 'vue-axios';
import VueMaterial from 'vue-material';
import VueSocketio from 'vue-socket.io';
import VueMoment from 'vue-moment';


import App from './App';
import router from './router';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/black-green-light.css';

import storeData from './store';

Vue.use(Vuex);
Vue.use(VueSocketio, 'http://localhost:3000');

const store = new Vuex.Store(storeData);
Vue.use(VueMaterial);
Vue.use(VueAxios, axios);
Vue.use(VueMoment);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: {
    App
  },
  template: '<App/>',
  sockets: {
    log(data) {
      store.commit('addLog', data);
      console.log('Log: ', data);
    },
    setSpin(data) {
      store.commit('setSpinning', data);
    }
  },
  created() {
    store.commit('setSpinning', true);
    store.dispatch('fetchUsers').then(() => store.dispatch('fetchGroups'));
    store.dispatch('fetchPosts');
    store.dispatch('fetchKeywords');
  }
});
