import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Index';
import Users from '@/components/Users';
import Posts from '@/components/Posts';
import Crawler from '@/components/Crawler';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/posts',
      name: 'Posts',
      component: Posts
    },

    {
      path: '/crawler',
      name: 'Crawler',
      component: Crawler
    }
  ]
});
