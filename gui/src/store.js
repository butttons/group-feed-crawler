import createPersistedState from 'vuex-persistedstate';
import axios from 'axios';
export default {
  //plugins: [createPersistedState()],
  state: {
    spinning: false,
    users: [],
    posts: [],
    groups: [],
    keywords: [],
    apiConsole: {
      status: '',
      log: []
    },
    selectedUserId: false
  },
  getters: {
    logOutput: state => state.apiConsole.log.join('\n'),
    logLength: state => state.apiConsole.log.length,
    userLength: state => state.users.length,
    selectedUser: state => {
      return state.selectedUserId
        ? state.users.filter(user => user.id === state.selectedUserId)[0]
        : state.users[0];
    },
    crawlingGroups: state => state.groups.filter(group => group.crawl === 1),
    keywordList: state => state.keywords.map(k => k.keyword)
  },
  mutations: {
    addLog(state, logText) {
      state.apiConsole.log.unshift(logText);
    },
    setUsers(state, usersData) {
      state.users = usersData;
    },
    setPosts(state, postsData) {
      state.posts = postsData;
    },
    setSpinning(state, spinning) {
      state.spinning = spinning;
    },
    setGroups(state, groupsData) {
      state.groups = groupsData;
    },
    setKeywords(state, keywordsData) {
      state.keywords = keywordsData;
    },
    setCrawlState(state, groupIds) {
      state.groups.forEach(group => {
        group.crawl = groupIds.includes(group.id) ? 1 : 0;
      });
    },
    setSelectedUserId(state, userId) {
      state.selectedUserId = userId;
    },
    clearLog(state) {
      state.apiConsole.log = [];
    }
  },
  actions: {
    async fetchUsers({ commit }) {
      commit('setSpinning', true);
      let resp = await axios.get('/api/users/get');
      if (resp.data.length) commit('setSelectedUserId', resp.data[0].id);
      commit('setUsers', resp.data);
      commit('addLog', 'Loaded users');
      commit('setSpinning', false);
    },
    async fetchPosts({ commit }) {
      commit('setSpinning', true);
      let resp = await axios.get('/api/facebook/posts');
      commit('setPosts', await resp.data);
      commit('addLog', 'Loaded posts');
      commit('setSpinning', false);
    },
    async fetchGroups({ commit, getters }) {
      commit('setSpinning', true);
      let resp = await axios.get(
        `/api/database/groups/${getters.selectedUser.facebook_id}`
      );
      commit('setGroups', resp.data);
      commit('addLog', 'Loaded groups');
      commit('setSpinning', false);
    },
    async fetchKeywords({ commit }) {
      commit('setSpinning', true);
      let resp = await axios.get('/api/database/keywords/get');
      commit('setKeywords', resp.data.keywords);
      commit('addLog', 'Loaded keywords');
      commit('setSpinning', false);
    }
  }
};
